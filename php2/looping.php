<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>looping</h1>
    <?php
    echo "<h5>Looping Pertama</h5>";
    echo "<h3> soal 1</h3>";
    $i = 2;
    while($i <= 20){
        echo " $i - Angka Genap 2-20 <br>";
        $i += 2;
    }

    echo "<h5>Looping Kedua</h5>";
    $a = 20;
    while($a >= 2) {
        echo "$a - Angka Genap 20-2 <br>";
        $a -= 2;
    }

    echo "<h3>soal 2</h3>";

    $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        // Lakukan Looping di sini
        foreach ($numbers as $angka) {
            $rest[] = $angka%=5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        echo "<br>";
        print_r($rest);

        echo "<h3>soal 3</h3>";

        $arrayMulti = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach($arrayMulti as $currenArray){
            $data = [
                "id" => $currenArray[0],
                "name" => $currenArray[1],
                "price" => $currenArray[2],
                "description" => $currenArray[3],
                "source" => $currenArray[4]
            ];
            print_r($data);
            echo "<br>";
        }

        echo "<h3>soal 4</h3>";

        for($j = 1; $j <= 5; $j++ ){
            for($b = 1; $b<=$j; $b++ ){
                echo "*";
            }
            echo "<br>";
        }






    ?>
</body>
</html>