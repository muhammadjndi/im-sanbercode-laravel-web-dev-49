<?php
class Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

public function __construct($nama){
    $this->name = $nama;
}
}


class Frog
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";
    public $jump = "Hop Hop";

public function __construct($nama){
    $this->name = $nama;
}
}


class Ape
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "no";
    public $yell = "Auooo";

public function __construct($nama){
    $this->name = $nama;
}
}

?>